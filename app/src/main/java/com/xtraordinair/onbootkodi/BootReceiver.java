package com.xtraordinair.onbootkodi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Created by Steph on 3/16/2017.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())){
            String mPrefsKey = "com.xtraordinair.onbootkodi_preferences";
            SharedPreferences mPrefs
                    = context.getSharedPreferences(mPrefsKey,Context.MODE_PRIVATE);
            boolean startOnBootEnabled
                    = mPrefs.getBoolean("key_start_kodi_on_boot",true);
            boolean timeoutServiceEnabled
                    = mPrefs.getBoolean("key_timeout_service", false);

            if(startOnBootEnabled){
                startKodi(context);
            }
            if(timeoutServiceEnabled) {
                context.startService(new Intent(context, ScreenService.class));
            }
        }else if(ScreenService.START_AFTER_TIMEOUT.equals(intent.getAction())){
            startKodi(context);
        }
    }

    private void startKodi(Context context){
        Intent launchIntent = context.getPackageManager()
                .getLaunchIntentForPackage("org.xbmc.kodi");
        if (launchIntent != null) {
            context.startActivity(launchIntent);
        }else{
            Toast.makeText(context, "Kodi not found!", Toast.LENGTH_LONG).show();
        }
    }
}
