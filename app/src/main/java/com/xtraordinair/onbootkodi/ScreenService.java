package com.xtraordinair.onbootkodi;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Created by Steph on 5/17/2017.
 */

public class ScreenService extends Service{

    public static String START_AFTER_TIMEOUT = "com.xtraordinair.onbootkodi.START_AFTER_TIMEOUT";
    private BroadcastReceiver mBroadcastReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        PendingIntent clickNotificationIntent = createPendingIntent();
        createNotification(clickNotificationIntent);
        registerReceiver();
        return START_STICKY;
    }

    private PendingIntent createPendingIntent() {
        Intent serviceKeepAlive = new Intent();
        return PendingIntent.getActivity(this,
                3,
                serviceKeepAlive,
                PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private void createNotification(PendingIntent intent) {

        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("OnBootKodi Service Running")
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(intent);
        NotificationManager notificationManager
                = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1001, mNotificationBuilder.build());

    }

    private void registerReceiver() {
        IntentFilter intentFiler = new IntentFilter();
        intentFiler.addAction(Intent.ACTION_SCREEN_OFF);
        intentFiler.addAction(Intent.ACTION_SCREEN_ON);

        mBroadcastReceiver = new BroadcastReceiver(){

            @Override
            public void onReceive(Context context, Intent intent) {
                if(Intent.ACTION_SCREEN_OFF.equals(intent.getAction())){
                    long timeRightNow = System.currentTimeMillis();
                    String mPrefsKey = "com.xtraordinair.onbootkodi_preferences";
                    SharedPreferences mPrefs
                            = context.getSharedPreferences(mPrefsKey,Context.MODE_PRIVATE);
                    SharedPreferences.Editor preferenceEdit = mPrefs.edit();
                    preferenceEdit.putLong("lastAwake", timeRightNow);
                    preferenceEdit.apply();

                }else if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())){
                    String mPrefsKey = "com.xtraordinair.onbootkodi_preferences";
                    SharedPreferences mPrefs
                            = context.getSharedPreferences(mPrefsKey,Context.MODE_PRIVATE);
                    long lastAwake = mPrefs.getLong("lastAwake", System.currentTimeMillis());
                    String timeoutPeriodString = mPrefs.getString("key_timeout_time", 5+"");
                    long timeoutPeriod = Long.parseLong(timeoutPeriodString);

                    //If asleep longer than 5 minutes (300000 milliseconds) then start Kodi
                    if(lastAwake + (timeoutPeriod * (60 * 1000)) < System.currentTimeMillis()) {
                        Intent startKodiAfterTimeout = new Intent(ScreenService.START_AFTER_TIMEOUT);
                        context.sendBroadcast(startKodiAfterTimeout);

                    }
                }
            }
        };

        this.registerReceiver(mBroadcastReceiver, intentFiler);
    }

    @Override
    public void onDestroy() {
        this.unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }
}
